window.onload = function(){
    var user_email = '';
    var user_name = '';
    var user_imgurl = '';
    var user_uid;
    var friend_num = 0;
    var people_uid = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('login-logout');
        if (user) {
            user_email = user.email;
            firebase.database().ref('/users/' + user.uid + '/name').once('value',function(snapshot){ 
                user_name = snapshot.val();
            })
            if(user.photoURL == null){
                document.getElementById("sidenav-img").src = 'profile.jpeg';
                user_imgurl = 'profile.jpeg';
            }
            else{
                document.getElementById("sidenav-img").src = user.photoURL;
                user_imgurl = user.photoURL;
            }
            user_uid = user.uid;
            load_friend_list();
            //user-picture + logout button
            menu.innerHTML = "<a class='navitem' href='#'>" + user_name + "<a class='navitem' href='#' id='logout-btn'><span class='glyphicon glyphicon-log-out'></span> Logout</a>";

            var friendAddbtn = document.getElementById("friend-add-btn");
             var friend_uid = document.getElementById("friend-uid");
             var friendRef = firebase.database().ref('friends/' + user_uid);
             if(user_uid != ''){
                 friendAddbtn.addEventListener('click',e => {
                     if (friend_uid.value != "") {
                        if(friend_uid.value != user_uid){
                        var is_id_exist = false;
                        firebase.database().ref('users').once('value',function(snapshot){
                            snapshot.forEach(function (childSnapshot) {
                                if(childSnapshot.key == friend_uid.value) is_id_exist = true;
                            });
                        }).then(function(){
                            if(is_id_exist){
                            var is_friend_repeat = false;
                            friendRef.once('value',function(snapshot){
                                snapshot.forEach(function (childSnapshot) {
                                    var childData = childSnapshot.val();
                                    if(childData.friend_uid == friend_uid.value) is_friend_repeat = true;
                                });
                            }).then(function(){
                                if(!is_friend_repeat){
                                friendRef.push({
                                    friend_uid: friend_uid.value
                                });
                               firebase.database().ref('friends/' + friend_uid.value).push({
                                   friend_uid: user_uid
                               }).then(function(){
                                   document.getElementById('friendList').innerHTML = '';
                                   friend_uid.value = '';
                                   alert('Add friend success!!!');
                                   document.location.href = "ChatRoom.html";
                               });
                                }
                                else alert('You guys are already friends!!!');
                            });
                            }
                            else alert("This ID doesn't exist!!!");
                        });
                    }
                    else alert("You can't add yourself as friend!!!");
                    }
                 });
             }

             //get friend list information
            function load_friend_list(){
                var ref = firebase.database().ref('friends/' + user_uid);
                ref.once('value').then(function(snapshot){
                    snapshot.forEach(function (childSnapshot){
                        var childData = childSnapshot.val();
                        var name;
                        var url;
                        firebase.database().ref('/users/' + childData.friend_uid).once('value',function(snapshot){ 
                            name = snapshot.val().name;
                            url = snapshot.val().imgurl;
                        }).then(function(){
                            friend_num++;             
                            var item = "<div style='position:relative; height:12.5%; width:90%; padding:3%;'>" + "<button name='" + name + "' " + "class='friend'" + " " + "value='" + childData.friend_uid + "'>" +
                            "<img style='position:relative; height:100%; width:40%; right:13%; border-radius: 50%;' src='" + url + "'>" + name + "</button>" + "</div>";
                            $('#friendList').append(item);
                        });
                    });
                });
                $('#friendList').on('click', '.friend', function(){
                    document.getElementById('message-area').innerHTML = '';
                    document.getElementById('right-top-block').innerHTML = this.name;
                    people_uid = this.value;
                    load_new_chat();
                    show_member_info();
                });
            }

            //chatting text upload
        var btnsendtext = document.getElementById("sendtextbtn");
        var chat_text = document.getElementById("chat-text");

            btnsendtext.addEventListener('click',e => {
            if (chat_text.value != "") {
                firebase.database().ref('chat_history').push({
                    sender: user_uid,
                    receiver: people_uid,
                    message: chat_text.value
                }).then(function(){chat_text.value = '';});
            }
            });

            var clearTextbtn = document.getElementById('cleartextbtn');
            clearTextbtn.addEventListener('click',e => {
                clear_text();
                document.getElementById('message-area').innerHTML = '';
            })

            //logout
            var btnlogout = document.getElementById("logout-btn");
            btnlogout.addEventListener('click',e => {
                firebase.auth().signOut();
                user_name = 'NULL';
                document.getElementById("sidenav-img").src = 'profile.jpeg';
                user_imgurl = 'profile.jpeg';
                user_uid = '';
                document.getElementById('friendList').innerHTML = '';
                document.getElementById('message-area').innerHTML = '';
                document.getElementById('ProfileCard').innerHTML = '';
            });
        } else {
            menu.innerHTML = "<a id='signup-btn' href='#'><span class='glyphicon glyphicon-user'></span> Sign Up</a>" + "<a href='#' id='login-btn'><span class='glyphicon glyphicon-log-in'></span> Login</a>";
            var btnlogin = document.getElementById('login-btn');
            var btnSignup = document.getElementById('signup-btn');
            btnlogin.addEventListener('click',e => {document.location.href = "LoginPage.html";});
            btnSignup.addEventListener('click',e => {document.location.href = "LoginPage.html";});
        }
    });

    function show_member_info(){
        var show_user_id = document.getElementById("show-user-id");
        var ProfileCard = document.getElementById("ProfileCard");
        var memberImg = document.getElementById("CardImg");
        var membername = document.getElementById("ProfileCard-name");
        var memberedu = document.getElementById("member-edu");
        var memberloca = document.getElementById("member-loca");
        var membergender = document.getElementById("member-gender");
        var memberphone = document.getElementById("member-phone");
        var memberbirth = document.getElementById("member-birth");
        firebase.database().ref('users/' + people_uid).once('value',function(snapshot){
            show_user_id.innerHTML = people_uid;
            memberImg.src = snapshot.val().imgurl;
            membername.innerHTML = snapshot.val().name;
            memberedu.innerHTML = snapshot.val().education;
            memberloca.innerHTML = snapshot.val().location;
            membergender.innerHTML = snapshot.val().gender;
            memberphone.innerHTML = snapshot.val().cellphone;
            memberbirth.innerHTML = snapshot.val().birthday;
            ProfileCard.style.visibility = 'visible';
        })
    }

    function clear_text(){
        var clearRef = firebase.database().ref('chat_history');
        clearRef.once('value',function(snapshot){
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                if((childData.sender == user_uid && childData.receiver == people_uid) || (childData.sender == people_uid && childData.receiver == user_uid)){
                    firebase.database().ref('chat_history/' + childSnapshot.key).remove();
                }
            });
        })
    }

    function load_new_chat(){
        var postsRef = firebase.database().ref('chat_history');
        var first_count = 0;
        var second_count = 0;
        if(user_uid != ''){
            postsRef.off();
            //load chatting text history
            var people_name;
            firebase.database().ref('users/' + people_uid).once('value',function(snapshot){
                people_name = snapshot.val().name;
            }).then(function(){
                postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        if(childData.sender == user_uid && childData.receiver == people_uid){
                            var message = "<div style='text-align:right; color:white; font-size:4vmin'>" + childData.message + "</div>";
                            $('#message-area').append(message);
                            first_count++;
                        }
                        else if(childData.sender == people_uid && childData.receiver == user_uid){
                            var message = "<div style='text-align:left; color:white; font-size:4vmin'>" + childData.message + "</div>";
                            $('#message-area').append(message);
                            first_count++;
                        }
                    });
                postsRef.on('child_added', function (data) {
                        var childData = data.val();
                        if((childData.sender == user_uid && childData.receiver == people_uid) || (childData.sender == people_uid && childData.receiver == user_uid)){
                        second_count++;
                        if(second_count>first_count){
                        if(childData.sender == user_uid && childData.receiver == people_uid){
                            var message = "<div style='text-align:right; color:white; font-size:4vmin'>" + childData.message + "</div>";
                            $('#message-area').append(message);
                        }
                        else if(childData.sender == people_uid && childData.receiver == user_uid){
                            var message = "<div style='text-align:left; color:white; font-size:4vmin'>" + childData.message + "</div>";
                            $('#message-area').append(message);
                        }
                        if(childData.receiver == user_uid){
                            createNotify(people_name);
                            notifyMe();
                        }
                        } 
                    }
                    });
                }).catch(e => console.log(e.message));
            });
        }
    }




    //sticky navbar
    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;
    
    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }

    // Case 1: 建立簡單的通知
var notifyConfig = {
    body: "Please checkout your chatroom!",
    icon: "https://cythilya.github.io/public/favicon.ico"
  }
  
  function createNotify(name) {
    if (!("Notification" in window)) { // 檢查瀏覽器是否支援通知
      console.log("This browser does not support notification");
    } else if (Notification.permission === "granted") { // 使用者已同意通知
      var notification = new Notification(
        name + " send a message!!", notifyConfig
      );
    } else if (Notification.permission !== "denied") {
      // 通知權限為 default (未要求) or undefined (老舊瀏覽器的未知狀態)，向使用者要求權限
      Notification.requestPermission(function(permission) {
        if (permission === "granted") {
          var notification = new Notification("Hi there!", notifyConfig);
        }
      });
    }
  }
  
  // Case 2: 加上標籤
  function notifyMe() {
    var tagList = ['newArrival', 'newFeature', 'newMsg', 'promotion'];
    var len = tagList.length;
    var times = 0;
    var timerNewArrival = setInterval(function() {
      sendNotify({
        title: tagList[times % len] + ' ' + times,
        body: '\\ ^o^ /',
        tag: tagList[times % len],
        icon: "https://cythilya.github.io/public/favicon.ico",
        url: 'http://www.ruten.com.tw/'
      });
      if (times++ == 20) {
        clearInterval(timerNewArrival);
      }
    }, 1000);
  }

    
    





};
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("page-wrap").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("page-wrap").style.marginLeft = "0";
    document.body.style.backgroundColor = "white";
}