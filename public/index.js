window.onload = function(){
    var user_email = '';
    var user_name = '';
    var user_imgurl = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('login-logout');
        if (user) {
            user_email = user.email;
            if(user.displayName == null || user.displayName == ''){
                user_name = 'NULL';
                firebase.database().ref('/users/' + user.uid).set({
                    name: 'NULL',
                    email: user.email,
                    education: 'NULL',
                    location: 'NULL',
                    gender: 'Male',
                    cellphone: 'NULL',
                    birthday: 'NULL',
                    intro: 'NULL',
                    group: 'NULL',
                    imgurl: 'profile.jpeg'
                });
                alert('Please complete your user information(Name is needed)!!!');
                document.location.href = "Profile_Edit.html";
            }
            else{
                user_name = user.displayName;
                firebase.database().ref('/users/' + user.uid).update({
                    name: user.displayName,
                    email: user.email
                });
            }
            if(user.photoURL == null){
                document.getElementById("sidenav-img").src = 'profile.jpeg';
                user_imgurl = 'profile.jpeg';
                alert('Please complete your user information(Profile picture is needed)!!!');
                document.location.href = "Profile_Edit.html";
            }
            else{
                document.getElementById("sidenav-img").src = user.photoURL;
                user_imgurl = user.photoURL;
                firebase.database().ref('/users/' + user.uid).update({
                    imgurl: user.photoURL
                });
            }
            menu.innerHTML = "<a class='navitem' href='#'>" + user_name + "<a class='navitem' href='#' id='logout-btn'><span class='glyphicon glyphicon-log-out'></span> Logout</a>";
          
            var btnlogout = document.getElementById('logout-btn');
            btnlogout.addEventListener('click',e => {
                firebase.auth().signOut();
                user_name = 'NULL';
                document.getElementById("sidenav-img").src = 'profile.jpeg';
                user_imgurl = 'profile.jpeg';
            });
        } else {
            menu.innerHTML = "<a id='signup-btn' href='#'><span class='glyphicon glyphicon-user'></span> Sign Up</a>" + "<a href='#' id='login-btn'><span class='glyphicon glyphicon-log-in'></span> Login</a>";
            var btnlogin = document.getElementById('login-btn');
            var btnSignup = document.getElementById('signup-btn');
            btnlogin.addEventListener('click',e => {document.location.href = "LoginPage.html";});
            btnSignup.addEventListener('click',e => {document.location.href = "LoginPage.html";});
        }
    });

    

    //sticky navbar
    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;
    
    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }



}

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("page-wrap").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("page-wrap").style.marginLeft = "0";
    document.body.style.backgroundColor = "white";
}