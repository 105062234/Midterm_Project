window.onload = function(){
    var user_email = '';
    var user_name = '';
    var user_imgurl = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('login-logout');
        var show_uid = document.getElementById('Card-content');
        if (user) {
            user_email = user.email;
            if(user.displayName == null){
                user_name = 'NULL';
            }
            else{
                user_name = user.displayName;
                document.getElementById("ProfileCard-name").innerHTML = user.displayName;
            }
            if(user.photoURL == null){
                document.getElementById("sidenav-img").src = 'profile.jpeg';
                user_imgurl = 'profile.jpeg';
            }
            else{
                document.getElementById("sidenav-img").src = user.photoURL;
                user_imgurl = user.photoURL;
            }
            menu.innerHTML = "<a class='navitem' href='#'>" + user_name + "<a class='navitem' href='#' id='logout-btn'><span class='glyphicon glyphicon-log-out'></span> Logout</a>";

            show_uid.innerHTML = "<p><i class='fas fa-id-card-alt'></i>" + "ID" + ":" + user.uid + "</p>";
          
            document.getElementById("CardImg").src = user_imgurl;

            var ref = firebase.storage().ref("userdata/" + user.uid + "/profile_img/" + "mypicture.jpg");
            var profile_image_upload = document.getElementById("profile-image-upload");
            profile_image_upload.addEventListener('change',e => {
                var imgfile = profile_image_upload.files[0];
                var reader = new FileReader();
                reader.readAsDataURL(imgfile);
                reader.addEventListener('load',function(e){
                    ref.putString(this.result,'data_url').then(function(snapshot) {
                        ref.getDownloadURL().then(function(url) {
                            user.updateProfile({
                                photoURL: url
                              }).then(function() {
                                firebase.database().ref('/users/' + user.uid).update({
                                    imgurl: url
                                });
                                document.getElementById("sidenav-img").src = url;
                                document.getElementById("CardImg").src = url;
                                document.getElementById("top-right-img").src = url;
                              }).catch(function(error) {
                                
                              });
                        });
                    });
                });
            })

            var btnchangeinfo = document.getElementById('confirm-change');
            var name_input = document.getElementById('name-input');
            var password_input = document.getElementById('password-input');
            var password_input_R = document.getElementById('password-input-R');
            var edu_input = document.getElementById('edu-input');
            var loca_input = document.getElementById('loca-input');
            var gender_input = document.getElementsByName('gender');
            var phone_input = document.getElementById('phone-input');
            var birth_input = document.getElementById('birth-input');
            var introduce_text = document.getElementById('introduce-text');
            var infoRef = firebase.database().ref('/users/' + user.uid);

            infoRef.once('value',function(snapshot){ 
                name_input.placeholder = snapshot.val().name;
                edu_input.placeholder = snapshot.val().education;
                loca_input.placeholder = snapshot.val().location;
                if(snapshot.val().gender == 'Male') gender_input[0].checked = true;
                else gender_input[1].checked = true;
                phone_input.placeholder = snapshot.val().cellphone;
                birth_input.value = snapshot.val().birthday;
                introduce_text.placeholder = snapshot.val().intro;
            })

            var password_flag = 1;

            btnchangeinfo.addEventListener('click',e => {
                if(name_input.value != ''){
                    infoRef.update({
                        name: name_input.value
                    }).then(function(){
                        user.updateProfile({
                            displayName: name_input.value
                        }).then(function() {
                            document.getElementById("ProfileCard-name").innerHTML = name_input.value;
                            name_input.placeholder = name_input.value;
                            name_input.value = '';
                        }).catch(function(error) {
                            // An error happened.
                        });
                    });
                }
                if(password_input.value.length>=6 && password_input.value == password_input_R.value){
                    user.updatePassword(password_input.value).then(function() {
                        password_input.placeholder = password_input.value;
                        password_input_R.placeholder = password_input_R.value;
                        password_input.value = '';
                        password_input_R.value = '';
                    }).catch(function(error) {});
                }
                else if(password_input.value != '' || password_input_R.value != ''){
                    password_flag = 0;
                    if(password_input.value.length<6) alert("Password character number must bigger than 6!!!");
                    else alert("Password you enter again doesn't match with the first one!!!");
                }
                if(edu_input.value != ''){
                    infoRef.update({
                        education: edu_input.value
                    }).then(function(){
                        edu_input.placeholder = edu_input.value;
                        edu_input.value = '';
                    });
                }
                if(loca_input.value != ''){
                    infoRef.update({
                        location: loca_input.value
                    }).then(function(){
                        loca_input.placeholder = loca_input.value;
                        loca_input.value = '';
                    });
                }
                if(gender_input[0].checked){
                    infoRef.update({
                        gender: 'Male'
                    }).then(function(){});
                }
                else{
                    infoRef.update({
                        gender: 'Female'
                    }).then(function(){});
                }
                if(phone_input.value != ''){
                    infoRef.update({
                        cellphone: phone_input.value
                    }).then(function(){
                        phone_input.placeholder = phone_input.value;
                        phone_input.value = '';
                    });
                }
                if(birth_input.value != ''){
                    infoRef.update({
                        birthday: birth_input.value
                    }).then(function(){
                        birth_input.placeholder = birth_input.value;
                    });
                }
                if(introduce_text.value != ''){
                    infoRef.update({
                        intro: introduce_text.value
                    }).then(function(){
                        introduce_text.placeholder = introduce_text.value;
                        introduce_text.value = '';
                    });
                }
                if(password_flag) alert('Change success!!!');
                else password_flag = 1;
            });

            var btnlogout = document.getElementById("logout-btn");
            btnlogout.addEventListener('click',e => {
                user_name = 'NULL';
                document.getElementById("sidenav-img").src = "profile.jpeg";
                document.getElementById("CardImg").src = "profile.jpeg";
                document.getElementById("ProfileCard-name").innerHTML = 'NULL';
                user_imgurl = 'profile.jpeg';
                firebase.auth().signOut();
            });
        } else {
            menu.innerHTML = "<a id='signup-btn' href='#'><span class='glyphicon glyphicon-user'></span> Sign Up</a>" + "<a href='#' id='login-btn'><span class='glyphicon glyphicon-log-in'></span> Login</a>";
            var btnlogin = document.getElementById('login-btn');
            var btnSignup = document.getElementById('signup-btn');
            btnlogin.addEventListener('click',e => {document.location.href = "LoginPage.html";});
            btnSignup.addEventListener('click',e => {document.location.href = "LoginPage.html";});
        }
    });

    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;
    
    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }


    
    





}
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("page-wrap").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("page-wrap").style.marginLeft = "0";
    document.body.style.backgroundColor = "white";
}