window.onload = function(){
    var user_email = '';
    var user_name = '';
    var user_imgurl = '';
    var user_uid = '';
    var GroupID = '';
    var GroupName = '';
    var MemberID = '';
    var MemberName = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('login-logout');
        if (user) {
            user_email = user.email;
            if(user.displayName == ''){
                user_name = 'NULL';
                //document.getElementById("").innerHTML = 'NULL';
            }
            else{
                user_name = user.displayName;
                //document.getElementById("").innerHTML = user.displayName;
            }
            if(user.photoURL == null){
                document.getElementById("sidenav-img").src = 'profile.jpeg';
                user_imgurl = 'profile.jpeg';
            }
            else{
                document.getElementById("sidenav-img").src = user.photoURL;
                user_imgurl = user.photoURL;
            }
            user_uid = user.uid;

            menu.innerHTML = "<a class='navitem' href='#'>" + user_name + "<a class='navitem' href='#' id='logout-btn'><span class='glyphicon glyphicon-log-out'></span> Logout</a>";

            load_group_list();

            var groupCreatebtn = document.getElementById("group-create-btn");
            var group_name = document.getElementById("create-group-name");
            var groupJoinbtn = document.getElementById("group-join-btn");
            var join_group_id = document.getElementById("join-group-id");
            var groupID = Math.random().toString(36).substring(6);
            var groupRef = firebase.database().ref('group/' + groupID);
            var group_member_Ref = firebase.database().ref('group/' + groupID + '/group_member');
            var userRef = firebase.database().ref('/users/' + user.uid + '/group');
            if(user_uid != ''){
                groupCreatebtn.addEventListener('click',e => {
                    if (group_name.value != "") {
                        groupRef.set({
                            group_name: group_name.value,
                            group_message: 'NULL',
                            group_member: 'NULL',
                            group_id: groupID
                        });
                        group_member_Ref.push({
                            member_id: user_uid,
                        }).then(function(){
                            userRef.push({
                            group_id: groupID
                            }).then(function(){
                                GroupID = groupID;
                                group_name.value = '';
                                var item = "<div>" + "Your group ID: " + groupID + "</div>";
                                $('#create-group-block').append(item);
                                document.getElementById('group-list').innerHTML = ''
                                document.getElementById('member-list').innerHTML = ''
                                alert("Create Success!!! Your group ID: " + groupID);
                                document.location.href = "group.html";
                            });
                        });
                    }
                });
                
                groupJoinbtn.addEventListener('click',e => {
                    if(join_group_id.value != ''){
                    var is_group_exist = false;
                    firebase.database().ref('group').once('value',function(snapshot){
                        snapshot.forEach(function (childSnapshot) {
                            var childData = childSnapshot.val();
                            if(childData.group_id == join_group_id.value) is_group_exist = true;
                        });
                    }).then(function(){
                        if (is_group_exist) {
                            var is_group_repeat = false;
                            userRef.once('value',function(snapshot){
                                snapshot.forEach(function (childSnapshot) {
                                var childData = childSnapshot.val();
                                if(childData.group_id == join_group_id.value) is_group_repeat = true;
                                });
                            }).then(function(){
                                if(!is_group_repeat){
                                    firebase.database().ref('group/' + join_group_id.value + '/group_member').push({
                                    member_id: user_uid
                                }).then(function(){
                                    userRef.push({
                                    group_id: join_group_id.value
                                    }).then(function(){
                                        join_group_id.value = '';
                                        document.getElementById('group-list').innerHTML = '';
                                        document.getElementById('member-list').innerHTML = '';
                                        alert("Join Success!!!");
                                        document.location.href = "group.html";
                                    });
                                });
                            }
                            else alert("You have been already in the Group!!!");
                            });
                        }
                        else alert("This group doesn't exist!!!");
                        });
                    }
                });
            }

            var btnsendtext = document.getElementById("sendtextbtn");
            var chat_text = document.getElementById("chat-text");
            btnsendtext.addEventListener('click',e => {
                if (chat_text.value != "") {
                    firebase.database().ref('group/' + GroupID + '/group_message').push({
                        sender: user_uid,
                        message: chat_text.value
                    });
                    chat_text.value = '';
                }
                });

            function load_group_list(){
            var grouplistref = firebase.database().ref('users/' + user_uid + '/group');
            grouplistref.once('value').then(function(snapshot){
                snapshot.forEach(function (childSnapshot){
                    var childData = childSnapshot.val();
                    var groupname;
                    firebase.database().ref('/group/' + childData.group_id + '/group_name').once('value',function(snapshot){ 
                        groupname = snapshot.val();
                    }).then(function(){  
                        var item = "<div id='groupbtndiv' style='position:relative; width:90%; padding:3%; left:5%;'>" + "<button name='" + groupname + "' " + " class='groupbtn'" + "value='" + childData.group_id + "'>"
                         + groupname + "</button>" + "</div>";
                        $('#group-list').append(item);
                    });
                });
            });
            $('#group-list').on('click', '.groupbtn', function(){
                document.getElementById('message-area').innerHTML = '';
                document.getElementById('member-list').innerHTML = '';
                document.getElementById('right-top-block').innerHTML = this.name + "(" + "GroupID: " + this.value + ")";
                GroupID = this.value;
                GroupName = this.name;
                load_new_chat();
                load_member_list();
            });
        }

        function load_member_list(){
                var member_name;
                var memberlistref = firebase.database().ref('group/' + GroupID + '/group_member');
                memberlistref.once('value').then(function(snapshot){
                snapshot.forEach(function (childSnapshot){
                    var childData = childSnapshot.val();
                    var name;
                    var url;
                    firebase.database().ref('/users/' + childData.member_id).once('value',function(snapshot){ 
                        name = snapshot.val().name;
                        url = snapshot.val().imgurl;
                    }).then(function(){
                        var item = "<div style='position:relative; height:30%; width:90%; padding:3%; left:5%'>" + "<button name='" + name + "' " + "class='member'" + " " + "value='" + childData.member_id + "'>" +
                        "<img style='position:relative; height:100%; width:40%; right:13%; border-radius: 50%;' src='" + url + "'>" + name + "</button>" + "</div>";
                        $('#member-list').append(item);
                    });
                });
            });
            $('#member-list').on('click', '.member', function(){
                MemberID = this.value;
                MemberName = this.name;
                show_member_info();
            });
        }
          

            var btnlogout = document.getElementById("logout-btn");
            btnlogout.addEventListener('click',e => {
                user_name = 'NULL';
                document.getElementById("sidenav-img").src = "profile.jpeg";
                document.getElementById('group-list').innerHTML = '';
                document.getElementById('message-area').innerHTML = '';
                document.getElementById('ProfileCard').innerHTML = '';
                document.getElementById('member-list').innerHTML = '';
                user_imgurl = 'profile.jpeg';
                firebase.auth().signOut();
            });
        } else {
            menu.innerHTML = "<a id='signup-btn' href='#'><span class='glyphicon glyphicon-user'></span> Sign Up</a>" + "<a href='#' id='login-btn'><span class='glyphicon glyphicon-log-in'></span> Login</a>";
            var btnlogin = document.getElementById('login-btn');
            var btnSignup = document.getElementById('signup-btn');
            btnlogin.addEventListener('click',e => {document.location.href = "LoginPage.html";});
            btnSignup.addEventListener('click',e => {document.location.href = "LoginPage.html";});
        }
    });

    function show_member_info(){
        var show_user_id = document.getElementById("show-user-id");
        var ProfileCard = document.getElementById("ProfileCard");
        var memberImg = document.getElementById("CardImg");
        var membername = document.getElementById("ProfileCard-name");
        var memberedu = document.getElementById("member-edu");
        var memberloca = document.getElementById("member-loca");
        var membergender = document.getElementById("member-gender");
        var memberphone = document.getElementById("member-phone");
        var memberbirth = document.getElementById("member-birth");
        firebase.database().ref('users/' + MemberID).once('value',function(snapshot){
            show_user_id.innerHTML = MemberID;
            memberImg.src = snapshot.val().imgurl;
            membername.innerHTML = snapshot.val().name;
            memberedu.innerHTML = snapshot.val().education;
            memberloca.innerHTML = snapshot.val().location;
            membergender.innerHTML = snapshot.val().gender;
            memberphone.innerHTML = snapshot.val().cellphone;
            memberbirth.innerHTML = snapshot.val().birthday;
            ProfileCard.style.visibility = 'visible';
        })
    }

    function load_new_chat(){
        var postsRef = firebase.database().ref('group/' + GroupID + '/group_message');
        var first_count = 0;
        var second_count = 0;

        if(user_uid != ''){
            postsRef.off();
            //load chatting text history
            var people_name;
            firebase.database().ref('users/' + MemberID).once('value',function(snapshot){
                people_name = snapshot.val().name;
            }).then(function(){
                postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        var name;
                        firebase.database().ref('users/' + childData.sender + '/name').once('value',function(snapshot){
                            name = snapshot.val();
                        }).then(function(){
                            var message = "<div style='text-align:left; color:white; font-size:4vmin'>" + name + ":" + childData.message + "</div>";
                            $('#message-area').append(message);
                            first_count++;
                        });
                    });
                postsRef.on('child_added', function (data) {
                        second_count++;
                        if(second_count>first_count){
                        var childData = data.val();
                        var name;
                        firebase.database().ref('users/' + childData.sender + '/name').once('value',function(snapshot){
                            name = snapshot.val();
                        }).then(function(){
                            var message = "<div style='text-align:left; color:white; font-size:4vmin'>" + name + ":" + childData.message + "</div>";
                            $('#message-area').append(message);
                            first_count++;
                        });
                        } 
                    });
                }).catch(e => console.log(e.message));
            });
        }
    }




    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;
    
    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }


    
    





};
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("page-wrap").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("page-wrap").style.marginLeft = "0";
    document.body.style.backgroundColor = "white";
}