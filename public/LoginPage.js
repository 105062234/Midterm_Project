function initApp() {
    var btnLogin = document.getElementById('sign-in-submit');
    var btnGoogle = document.getElementById('google-sign-in');
    var btnFacebook = document.getElementById('facebook-sign-in');
    var btnSignUp = document.getElementById('sign-up-submit');

    btnLogin.addEventListener('click', function () {
        var email = document.getElementById('email-sign-in').value;
        var password = document.getElementById('passwd-sign-in').value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function () {
            document.location.href = "index.html";
        }).catch(e => alert(e.message));
    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            document.location.href = "index.html";
        }).catch(function (error) {
            alert(error.message)
        });
    });

    btnFacebook.addEventListener('click', function () {
        var facebook_provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(facebook_provider).then(function (result) {
            document.location.href = "index.html";
        }).catch(function (error) {
            alert(error.message)
        });
    });

    btnSignUp.addEventListener('click', function () {        
        var email = document.getElementById('email-sign-up').value;
        var password = document.getElementById('passwd-sign-up').value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function () {
            document.location.href = "index.html";
        }).catch(e => alert(e.message));
    });
}

window.onload = function () {
    initApp();
};
      