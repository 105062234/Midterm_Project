# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Chatroom
* Key functions (add/delete)
    1. 可以進行1對1私人傳訊(僅限於好友之間)
    2. 進行過的對話會保存(也有clerr鍵可以刪除)
    3. 可以點擊好友列表的好友來查看好友資料
    4. 可以透過左上角輸入帳戶ID來新增好友(已是朋友或ID不存在都不會新增)
* Other functions(Group) (add/delete)
    1. 可以進行多人的群組對話
    2. 左上角可以創建群組(創建時會隨機產生一組ID來供其他人加入)
    3. 輸入的群組ID不存在或已加入都不會加入
    4. 群組列表的下方有成員列表，點擊可以查看個人資料

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
1. 首頁為一個簡單的landing page，展示三張宣傳用的圖
2. 登入及其他功能選單都在左邊隱藏起來的side navbar中，點擊左上角可以展開選單
3. 選單展開的過程是利用css動畫屬性達成的(包括展開時把整個頁面推向右邊)
4. 在編輯個人資料的頁面可以更改密碼和上傳頭像
5. 預設最基本要上傳完頭像和新增名字才能繼續使用其他功能(不然會一直被導向到編輯頁面)
6. 個人ID及其他更改後的資訊會都顯示在個人編輯頁面中
7. 一旦登出所有功能將不能使用，且畫面會切到預設狀態
8. 所有頁面皆有做RWD

## Security Report (Optional)
#對寫入使用者個人資料時，加強安全措施，僅有自己才能寫入自己的個人資料，個人資料的讀取則是需要登入才能存取
